/**********************************************
* File: AVLTest.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/
#include <iostream>
#include "AVLTree.h"

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* ABL function 
********************************************/
int main(int argc, char **argv)
{
    AVLTree<int> testAVL;
    
	std::cout << "Testing testAVL" << std::endl;
	testAVL.insert( 10 );
	testAVL.insert( 20 );
	testAVL.insert( 30 );
	testAVL.insert( 30 );  // Test for no duplicates
	testAVL.insert( 5 );
	testAVL.insert( 4 );
	
	testAVL.printTree( );
	
	std::cout << "The maximum value is " << testAVL.findMax() << std::endl;
	std::cout << "The minimum value is " << testAVL.findMin() << std::endl;
	
	std::cout << "The test for whether testAVL contains 10 returns " << testAVL.contains( 10 ) << std::endl;
	std::cout << "The test for whether testAVL contains 18 returns " << testAVL.contains( 18 ) << std::endl;
	
	std::cout << "Testing testAVL2 after removing 20" << std::endl;
	AVLTree<int> testAVL2(testAVL);
	testAVL2.remove(20);
	testAVL2.printTree( );
	std::cout << "The test for whether testAVL2 contains 20 returns " << testAVL2.contains( 20 ) << std::endl;
	
    return 0;
}
