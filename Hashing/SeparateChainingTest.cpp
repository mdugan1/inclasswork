/**********************************************
* File: SeparateChainingTest.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "SeparateChaining.h"

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*  
********************************************/
int main(int argc, char** argv)
{
	// Initialize a Hash Table with a Size of 8
    HashTable<int> hTable(8);

	hTable.insert(4); 
	hTable.insert(13); 
	hTable.insert(8); 
	hTable.insert(10); 
	hTable.insert(5); 
	hTable.insert(15);
	
	hTable.printHash(std::cout);
	
	std::cout << "------" << std::endl;
	std::cout << "Table after removing 13" << std::endl;
	hTable.remove(13);
	
	hTable.printHash(std::cout);
	
	std::cout << "------" << std::endl;
	std::cout << "Inserting 50 Random Numbers between 0 and 50...";
	// Seed the RNG once, at the start of the program
	srand( time( NULL ) );
	
	for(int ins = 0; ins < 50; ins++){
		hTable.insert(rand() % 50);
	}
	
	std::cout << "Table after insertion: " << std::endl;
	hTable.printHash(std::cout);

    return 0;
}
